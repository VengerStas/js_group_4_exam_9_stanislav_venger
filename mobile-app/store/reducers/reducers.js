import {CONTACTS_ERROR, CONTACTS_REQUEST, CONTACTS_SUCCESS, TOGGLE_MODAL} from "../actions/actionTypes";

const initialState = {
    contacts: {},
    showModal: false,
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                showModal: !state.showModal,
            };
        case CONTACTS_REQUEST:
            return {
                ...state,
            };
        case CONTACTS_SUCCESS:
            return {
                ...state,
                contacts: action.contacts,
            };
        case CONTACTS_ERROR:
            return {
                ...state,
            };
        default:
            return state;
    }
};

export default reducer;
