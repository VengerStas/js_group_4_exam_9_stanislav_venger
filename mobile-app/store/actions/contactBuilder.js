import axios from '../../axiosBase';
import {CONTACTS_ERROR, CONTACTS_REQUEST, CONTACTS_SUCCESS, TOGGLE_MODAL} from "./actionTypes";

export const showModalWindow = () => {
    return {type: TOGGLE_MODAL};
};

export const contactRequest = () => {
    return {type: CONTACTS_REQUEST};
};

export const contactSuccess = contacts => {
    return {type: CONTACTS_SUCCESS, contacts};
};

export const contactError = error => {
    return {type: CONTACTS_ERROR, error};
};

export const contactsList = () => {
    return (dispatch) => {
        dispatch(contactRequest());
        axios.get('contacts.json').then(response => {
            dispatch(contactSuccess(response.data));
        }, error => {
            dispatch(contactError(error));
        });
    }
};
