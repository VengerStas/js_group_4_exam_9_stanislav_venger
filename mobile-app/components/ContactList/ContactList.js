import React, {Component} from 'react';
import {connect} from "react-redux";
import {FlatList, StyleSheet, Text, View, TouchableOpacity, Image, Modal} from "react-native";
import {contactsList, showModalWindow} from "../../store/actions/contactBuilder";

class ContactList extends Component {
    state = {
        modal: false
    };

    showModal = id => {
        this.setState({modal:id})
    };

    hideModal = () => {
        this.setState({modal: false})
    };

    componentDidMount() {
        this.props.contactsList();
    }

    render() {
        let currentContact = Object.keys(this.props.contacts).map(contact => {
            return {
                id: contact,
                ...this.props.contacts[contact]
            }
        });

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>Contacts</Text>
                </View>
                <FlatList
                    data={currentContact}
                    keyExtractor={(contact) => contact.id}
                    renderItem={({item}) => (
                        <TouchableOpacity style={styles.contactBlock} onPress={() => this.showModal(item.id)}>
                            <Image
                                source={{uri: item.photo}}
                                style={styles.image}
                            />
                            <Text style={styles.text}>{item.name}</Text>
                            <Modal
                                animationType="fade"
                                transparent={false}
                                visible={!!this.state.modal}
                                onRequestClose={this.hideModal}
                            >
                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={styles.text}>{item.name}</Text>
                                    <Image
                                        source={{uri: item.photo}}
                                        style={styles.image}
                                    />
                                    <Text style={styles.text}>{item.phone}</Text>
                                    <Text style={styles.text}>{item.email}</Text>
                                </View>
                            </Modal>
                        </TouchableOpacity>
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        textAlign: 'center',
        paddingTop: 25,
        fontSize: 20
    },
    header: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'darkorange',
        paddingTop: 20,
        paddingBottom: 30,
        marginBottom: 10
    },
    headerText: {
        fontWeight: 'bold'
    },
    contactBlock: {
        flex: 1,
        justifyContent: 'center',
        marginBottom: 10
    },
    image: {
        flex: 3,
        alignSelf: 'center',
        width: 150,
        height: 150,
        marginBottom: 20
    },
    text: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 20,
        paddingVertical: 10
    }
});

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        contactsList: () => dispatch(contactsList()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
