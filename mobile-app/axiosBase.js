import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://contacts-d5a45.firebaseio.com/'
});

export default instance;
