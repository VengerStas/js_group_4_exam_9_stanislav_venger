import React from 'react';
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import reducer from "./store/reducers/reducers";
import {Provider} from "react-redux";
import ContactList from "./components/ContactList/ContactList";


const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const app = () =>  (
    <Provider store={store}>
      <ContactList/>
    </Provider>
);

export default app;



