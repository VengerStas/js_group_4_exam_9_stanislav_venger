import React, {Component} from 'react';
import ContactsForm from "../../components/ContactsForm/ContactsForm";

import './EditContacts.css';
import {connect} from "react-redux";
import {editContact, saveEditContact} from "../../store/actions/contactsBuilder";

class EditContacts extends Component {

    componentDidMount () {
        this.props.editContact(this.props.match.params.id);
    }

    editCurrentDish = (contactEdit) => {
        this.props.saveEditContact(this.props.match.params.id, contactEdit);
        this.props.history.push('/');
    };

    backToHandler = () => {
        this.props.history.push('/');
    };

    render() {
        let form = <ContactsForm
            onSubmit={this.editCurrentDish}
            contact={this.props.contact}
            cancel={this.backToHandler}
        />;

        if (!this.props.contact) {
            form = <div>Loading Data...</div>
        }

        return (
            <div>
                <h4 className="edit-page">Edit contact info</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        contact: state.contact,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editContact: (contactId) => dispatch(editContact(contactId)),
        saveEditContact: (contactId, contact) => dispatch(saveEditContact(contactId, contact)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContacts);
