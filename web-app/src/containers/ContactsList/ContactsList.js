import React, {Component, Fragment} from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, Container, NavLink, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import {deleteContact, fetchGetContacts} from "../../store/actions/contactsBuilder";

import './ContactsList.css';

class ContactsList extends Component {
    state = {
        modal: false
    };

    showModal = id => {
        this.setState({modal:id})
    };

    hideModal = () => {
        this.setState({modal: false})
    };

    deleteHandler = (contactId) => {
        this.props.deleteContact(contactId);
        this.hideModal();
        this.props.history.push('/');
    };

    componentDidMount() {
        this.props.loadContacts();
    };

    render() {
        const closeBtn = <button className="close" onClick={this.hideModal}>&times;</button>;
        let contact = null;
        if (this.props.contacts) {
            contact = Object.keys(this.props.contacts).map(id => {
                return (
                    <Fragment key={id}>
                        <div className="contact" onClick={() => this.showModal(id)}>
                            <img src={this.props.contacts[id].photo} alt={this.props.contacts[id].name} className="image"/>
                            <p className="contact-name">{this.props.contacts[id].name}</p>
                        </div>
                        <Modal isOpen={this.state.modal === id} toggle={this.hideModal} className={this.props.className}>
                            <ModalHeader toggle={this.hideModal} close={closeBtn}>Contact info</ModalHeader>
                            <ModalBody>
                                <img src={this.props.contacts[id].photo} alt={this.props.contacts[id].name} className="modal-img"/>
                                <p>{this.props.contacts[id].name}</p>
                                <p>{this.props.contacts[id].phone}</p>
                                <p>{this.props.contacts[id].email}</p>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" tag={RouterNavLink} to={id + "/edit"}>Edit</Button>{' '}
                                <Button color="secondary" onClick={() => this.deleteHandler(id)}>Delete</Button>
                            </ModalFooter>
                        </Modal>
                    </Fragment>
                )
            });
        }

        return (
            <div>
                <Navbar color="light" light expand="md">
                    <Container>
                        <NavbarBrand>Contacts</NavbarBrand>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink className="add-contact" tag={RouterNavLink} to="/add-contacts">Add contacts</NavLink>
                            </NavItem>
                        </Nav>
                    </Container>
                </Navbar>
                <div className="contacts-block">
                    <Container>
                        {contact}
                    </Container>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
    loadContacts: () => dispatch(fetchGetContacts()),
    deleteContact: (contactId) => dispatch(deleteContact(contactId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);
