import React, {Component} from 'react';
import ContactsForm from "../../components/ContactsForm/ContactsForm";
import {connect} from "react-redux";
import {fetchPostContact} from "../../store/actions/contactsBuilder";

class AddContacts extends Component {

    addNewContact = (contact) => {
        this.props.sendContacts(contact);
        this.props.history.push('/');
    };

    backToHandler = () => {
        this.props.history.push('/');
    };

    render() {
        return (
            <div>
                <h4 className="add-title">Add new contact</h4>
                <ContactsForm
                    onSubmit={this.addNewContact}
                    cancel={this.backToHandler}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
    sendContacts: (contact) => dispatch(fetchPostContact(contact)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContacts);
