import {
    CANCEL_CONTACT,
    CONTACT_REQUEST_SUCCESS,
    CONTACTS_REQUEST_SUCCESS,
    DELETE_CONTACTS
} from "../actions/actionTypes";

const initialState = {
    contacts: {},
    contact: null,
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONTACTS_REQUEST_SUCCESS:
            return {
                ...state,
                contacts: action.contacts,
            };
        case CONTACT_REQUEST_SUCCESS:
            return {
                ...state,
                contact: action.contact
            };
        case DELETE_CONTACTS:
            return {
                ...state,
            };
        case CANCEL_CONTACT:
            return {
                ...state,
                contacts: action.contacts
            };
        default:
            return state;
    }
};

export default contactsReducer;
