import axios from '../../axiosBase';
import {
    CANCEL_CONTACT,
    CONTACT_REQUEST_SUCCESS,
    CONTACTS_FAILURE,
    CONTACTS_REQUEST,
    CONTACTS_REQUEST_SUCCESS
} from "./actionTypes";

export const contactsRequest = () => {
    return {type: CONTACTS_REQUEST};
};

export const contactsSuccess = contacts => {
    return {type: CONTACTS_REQUEST_SUCCESS, contacts};
};

export const contactsFailure = error => {
    return {type: CONTACTS_FAILURE, error};
};

export const cancel = (contacts) => {
  return {type: CANCEL_CONTACT, contacts}
};

export const fetchPostContact = (contact) => {
    return (dispatch) => {
        dispatch(contactsRequest());
        axios.post('contacts.json', contact).then(() => {
            dispatch(contactsRequest());
            dispatch(fetchGetContacts());
        }, error => {
            dispatch(contactsFailure(error));
        })
    }
};

export const fetchGetContacts = () => {
    return (dispatch) => {
        dispatch(contactsRequest());
        axios.get('contacts.json').then(response => {
            dispatch(contactsSuccess(response.data));
        }, error => {
            dispatch(contactsFailure(error));
        });
    }
};

export const deleteContact = (contactId) => {
    return (dispatch) => {
        dispatch(contactsRequest());
        axios.delete('contacts/' + contactId + '.json').then(() => {
            dispatch(fetchGetContacts());
        })
    }
};

export const editContact = (contactId) => {
    return (dispatch) => {
        dispatch(contactsRequest());
        axios.get('contacts/' + contactId + '.json').then((response) => {
            dispatch({type: CONTACT_REQUEST_SUCCESS, contact: response.data});
        })
    }
};

export const saveEditContact = (contactId, contact) => {
    return (dispatch) => {
        dispatch(contactsRequest());
        axios.put('contacts/' + contactId + '.json', contact).then(() => {
            dispatch(fetchGetContacts());
        })
    }
};
