import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import ContactsList from "./containers/ContactsList/ContactsList";
import AddContacts from "./containers/AddContacts/AddContacts";
import EditContacts from "./containers/EditContacts/EditContacts";

class App extends Component {
  render() {
    return (
        <Switch>
          <Route path='/' exact component={ContactsList} />
          <Route path='/add-contacts' exact component={AddContacts}/>
          <Route path='/:id/edit' exact component={EditContacts}/>
          <Route render={() => <h1>Page not found</h1>}/>
        </Switch>
    );
  }
}

export default App;
