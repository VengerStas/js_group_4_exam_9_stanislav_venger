import React, {Component} from 'react';
import { Form, FormGroup, Label, Col, Container, Input, Button} from 'reactstrap';

import './ContactsForm.css';

class ContactsForm extends Component {
    constructor(props) {
        super (props);
        if (this.props.contact) {
            this.state = {...props.contact}
        } else {
            this.state = {
                name: '',
                phone: '',
                email: '',
                photo: '',
            };
        }
    }

    getInputValue = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    backToHandler = () => {
        this.props.history.push('/');
    };

    saveContactHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Container>
                <Form className="contact-form" onSubmit={this.saveContactHandler}>
                    <FormGroup row>
                        <Label for="name" sm={2}>Name:</Label>
                        <Col sm={10}>
                            <Input type="text" name="name" id="name" placeholder="Enter contact name"
                                    value={this.state.name} onChange={this.getInputValue} required/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="phone" sm={2}>Phone:</Label>
                        <Col sm={10}>
                            <Input type="tel" name="phone" id="phone" placeholder="Enter phone number"
                                   value={this.state.phone} onChange={this.getInputValue} required/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="email" sm={2}>Email:</Label>
                        <Col sm={10}>
                            <Input type="email" name="email" id="email" placeholder="example@gmail.com"
                                   value={this.state.email} onChange={this.getInputValue} required/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="photo" sm={2}>Photo:</Label>
                        <Col sm={10}>
                            <Input type="url" name="photo" id="photo" placeholder="Enter contact's photo url"
                                   value={this.state.photo} onChange={this.getInputValue}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="photo" sm={2}>Photo preview:</Label>
                        <Col sm={10}>
                            <div className="preview-photo">
                                {this.state.photo ? <img src={this.state.photo} alt="contact" className="preview-photo-img"/> : null}
                            </div>
                        </Col>
                    </FormGroup>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                            <Button type="submit" className="btn save">Save</Button>
                            <Button className="btn back" onClick={this.props.cancel}>Back to contacts</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Container>
        );
    }
}


export default ContactsForm;
